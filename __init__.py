import signal
import multiprocessing
import traceback

def map(fn, iters, processes=None, limit=None, *arg):
    '''
    TODO Add log file, as in comand line PFS
    '''

    if processes is None:
        if multiprocessing.cpu_count()>1:
            processes = multiprocessing.cpu_count()-1
        else:
            processes = 1

    if not limit is None:
        print('PFS: Limit to {} files'.format(limit))

    if processes>multiprocessing.cpu_count():
        print('WARNING: Selected number of cores bigger than the actual number of cores available, forcing to the number of cores.')
        processes = multiprocessing.cpu_count()
    print('PFS: Using {} cores'.format(processes))

    if processes==1:
        for it in iters:
            fn(it)

    else:
        original_sigint_handler = signal.signal(signal.SIGINT, signal.SIG_IGN)
        pool = multiprocessing.Pool(processes=processes)
        signal.signal(signal.SIGINT, original_sigint_handler)

        try:
        # #result = pool.map(run_cmd, cmds)
            result = pool.map_async(fn, iters)
            # result = pool.map(fn, iters)
            result.get(float('inf')) # TODO Seems a working hack. I cannot use None otherwise the signals are not caught (https://stackoverflow.com/questions/11312525/catch-ctrlc-sigint-and-exit-multiprocesses-gracefully-in-python/11312948)
        except KeyboardInterrupt:
            print("PFS::map: Caught KeyboardInterrupt by user")
            pool.terminate()
        except Exception as e:
            print("PFS::map: Exception caught:")
            print(e.__doc__)
            print(e.message)
            traceback.print_exc()
            pool.terminate()
        else:
            pool.close()
        pool.join()

        # result = pool.map(fn, iters) # This one can't be interrupted properly
