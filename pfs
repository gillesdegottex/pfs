#!/usr/bin/python

# Parallel File Scripting (PFS)

# TODO Log failed commands
# TODO SGE grid

import sys
import os
import argparse
import glob
import string
import errno

import multiprocessing

import shlex
from subprocess import Popen, PIPE, STDOUT

import signal
#def signal_handler(signal, frame):
        #print('PFS terminated by user')
        #sys.exit(0)
#signal.signal(signal.SIGINT, signal_handler)

global pfsargs
global flog

def makedirs(path):
    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise


def usage(name=None):
    return '''pfs [-h] -i indir1 inext1 [indir2 inext2 ...] -o outdir1 outext1 [outdir2 outext2 ...] -s command {i1} {o1}'''

def run_cmd(cmd):
    print('PFS: Run: '+cmd)

    global flog
    if not flog is None:
        flog.write(cmd+'\n')

    args = shlex.split(cmd)
    stdout = PIPE
    if args[-2]=='>':   # TODO Deal with simple redirection to file (it might be way more complicate: might need to run the command with shell=True)
        stdout = open(args[-1], 'w')
        args = args[:-2]
    proc = Popen(args, stdout=stdout, stderr=STDOUT)
    out, err = proc.communicate()
    ret = proc.returncode
    if not flog is None:
        flog.write(out)
        flog.flush()
    if ret!=0:
        print(out)

    if ret!=0:
        print('WARNING: Command returned: '+str(ret))

def build_fcmd(cmd, ins, ous, ff):
    fbase = os.path.splitext(os.path.basename(ff))[0]

    fcmd = cmd
    #i1 = ff
    #fcmd = string.replace(fcmd, "{i1}", i1)

    for inn in range(len(ins)):
        if inn%2==0:
            ii = os.path.join(ins[inn], fbase+ins[inn+1])
            fcmd = string.replace(fcmd, '{i'+str(inn/2+1)+'}', ii)

    for on in range(len(ous)):
        if on%2==0:
            oo = os.path.join(ous[on], fbase+ous[on+1])
            fcmd = string.replace(fcmd, '{o'+str(on/2+1)+'}', oo)

    return fcmd


if  __name__ == "__main__" :

    if multiprocessing.cpu_count()>1: mc_def=multiprocessing.cpu_count()-1
    else:                             mc_def=1

    argpar = argparse.ArgumentParser(usage=usage())
    argpar.add_argument("--mc", type=int, default=mc_def, help="Use multiple CPU [def. use number of CPU-1]")
    argpar.add_argument("-l", "--limit", type=int, default=None, help="Limit to N files (usefull for debugging the command)")
    argpar.add_argument("--log", default=None, help="Log computations output to the given file")
    argpar.add_argument("--log_quiet", action='store_true', help="Do not print PFS information in log")
    # TODO Add custom help
    global pfsargs
    try:
        pfsargs = sys.argv[:sys.argv.index('-s')+1]
    except ValueError, e:
        pfsargs = sys.argv
    pfsargs, unknown = argpar.parse_known_args(pfsargs)

    if not pfsargs.limit is None:
        print('PFS: Limit to {} files'.format(pfsargs.limit))

    ins = list()
    ous = list()
    cmd = list()
    na = 1

    # Reach '-i'
    while na<len(sys.argv) and sys.argv[na]!='-i':
        na += 1
    na += 1 # Skip '-i'
    # List the inputs until it reaches '-o' or '-s'
    while na<len(sys.argv) and (sys.argv[na]!='-o' and sys.argv[na]!='-s'):
        ins.append(sys.argv[na])
        na += 1
    has_outputs = sys.argv[na]=='-o'
    na += 1 # Skip '-o' or '-s'
    if has_outputs:
        # List the outputs until it reaches '-s'
        while na<len(sys.argv) and sys.argv[na]!='-s':
            ous.append(sys.argv[na])
            na += 1
        na += 1 # Skip '-s'
    # All the rest is the command line to be executed
    while na<len(sys.argv):
        cmd.append(sys.argv[na])
        na += 1
    cmd = ' '.join(cmd)

    print('PFS: Input: '+str(ins))
    print('PFS: Output: '+str(ous))

    for fou in ous:
        if fou[0]=='.':
            continue
        makedirs(fou)

    print('PFS: Command: '+cmd)

    if len(ins)<1:
        print(usage())
    fin = ins[0]
    if len(ins)>1:
        fin = os.path.join(fin,'*'+ins[1])

    print('PFS: Input wildcard for the basenames: '+fin)

    global flog
    flog = None
    if not pfsargs.log is None:
        print('PFS: Log command outputs to: '+pfsargs.log)
        flog = open(pfsargs.log, 'a')
        if not pfsargs.log_quiet:
            flog.write('PFS: Input: '+str(ins)+'\n')
            flog.write('PFS: Output: '+str(ous)+'\n')
            flog.write('PFS: Command: '+cmd+'\n')
            flog.write('PFS: Input wildcard for the basenames: '+fin+'\n')
            flog.write('PFS: Start\n')
        flog.flush()

    if not pfsargs.mc is None:
        if pfsargs.mc>multiprocessing.cpu_count():
            print('WARNING: Selected number of CPUs bigger than the actual number of CPUs available, forcing to the number of CPUs.')
            pfsargs.mc = multiprocessing.cpu_count()
        print('PFS: Using {} CPUs'.format(pfsargs.mc))

        cmds = list()
        limit = 1
        for ff in glob.glob(fin):
            if not pfsargs.limit is None and limit>pfsargs.limit: continue
            limit += 1

            fcmd = build_fcmd(cmd, ins, ous, ff)

            cmds.append(fcmd)

        original_sigint_handler = signal.signal(signal.SIGINT, signal.SIG_IGN)
        pool = multiprocessing.Pool(processes=pfsargs.mc)
        signal.signal(signal.SIGINT, original_sigint_handler)

        try:
        #result = pool.map(run_cmd, cmds)
            result = pool.map_async(run_cmd, cmds)
            result.get(float('inf')) # TODO Seems a working hack. I cannot use None otherwise the signals are not caught (https://stackoverflow.com/questions/11312525/catch-ctrlc-sigint-and-exit-multiprocesses-gracefully-in-python/11312948)
        except KeyboardInterrupt:
            print("PFS::map: Caught KeyboardInterrupt by user")
            pool.terminate()
        except Exception as e:
            print("PFS::map: Exception caught:")
            print(e.__doc__)
            print(e.message)
            traceback.print_exc()
            pool.terminate()
        else:
            pool.close()
        pool.join()

    else:
        print('PFS: Using a single CPU')
        limit = 1
        for ff in glob.glob(fin):
            if not pfsargs.limit is None and limit>pfsargs.limit: continue
            limit += 1

            fcmd = build_fcmd(cmd, ins, ous, ff)

            run_cmd(fcmd)

    if not flog is None:
        if not pfsargs.log_quiet:
            flog.write('PFS: Finished\n')
        flog.close()
